let time = 0;
let blocks = undefined;
let heights = undefined;
let gamePlayPossition = 0;
let gamePlayMovedTimes = 0;

let result = 0;

const interval = 16;

let keys = new Map();
const jumpKeys = ['Space', 'KeyW', 'ArrowUp'];
const fallKeys = ['KeyS', 'ArrowDown'];

let jump = false;
let fall = false;

const fallStartSpeed = -300.0;


let playerHeight = 60.0;
let verticalSpeed = 0.0;
const startVerticalSpeed = 500.0;
const g = -1000.0;

function start() {
    moveBlocks();
    displayBlocks();
    updatePlayer();
    updateTime();
    updateScore();
}

function moveBlocks() {
    let gamePlay = document.getElementById("gamePlay");
    if (blocks == undefined) {
        blocks = [];
        heights = [];
        for (j = 0; j < 10; j++) {
            block = [];
            for (i = 0; i < 31; i++) {
                let d = document.createElement("DIV");
                if (j < 8) {
                    d.className = "block";
                } else {
                    d.className = "block green";
                }
                gamePlay.appendChild(d);
                block.push(d);
            }
            blocks.push(block);
        }
        for (i = 0; i < 31; i++) {
            heights.push(2);
        }
    } else {
        if (checkCollisionX()) {
            clearInterval(intervalId);
            return;
        }
        if (gamePlayPossition >= 30) {
            gamePlayPossition = 0;
            gamePlayMovedTimes++;
            let newHeight = Math.floor(Math.random() * 6) + 2;
            for (let j = 0; j < 10; j++) {
                for (let i = 0; i < 30; i++) {
                    blocks[j][i].className = blocks[j][i + 1].className;
                }
                if (j < 10 - newHeight) {
                    blocks[j][30].className = "block";
                } else {
                    blocks[j][30].className = "block green";
                }
            }
            for (i = 0; i < 30; i++) {
                heights[i] = heights[i + 1];
            }
            heights[30] = newHeight;
        } else {
            if (gamePlayMovedTimes < 10) {
                gamePlayPossition += 1;     
            } else if (gamePlayMovedTimes < 25) {
                gamePlayPossition += 2;
            } else if (gamePlayMovedTimes < 50) {
                gamePlayPossition += 3;
            } else {
                gamePlayPossition += 5;
            }
        }
        gamePlay.style.left = -gamePlayPossition + 'px';
    }
}

function displayBlocks() {

}

function updatePlayer() {
    const startHeight = heights[4] * 30;
    
    if (!jump && playerHeight == startHeight) {
        return;
    }

    if (fall && (verticalSpeed > fallStartSpeed)) {
        verticalSpeed = fallStartSpeed;
    }

    playerHeight += verticalSpeed * (interval / 1000.0);
    verticalSpeed += g * (interval / 1000.0);

    if (playerHeight + 15 < startHeight) {
        
        clearInterval(intervalId);
        return;
    } else if (playerHeight < startHeight) {
        playerHeight = startHeight;
        
        if (jump) {
            verticalSpeed = startVerticalSpeed;
        }

    }

    let player = document.getElementById("player");
    player.style.bottom = Math.round(playerHeight) + 'px';
    
}

function updateTime() {
    time += Math.min(Math.floor(Math.pow(time, 0.27) + 10), 50);
}

/** updates score == time / 200 */
function updateScore() {
    let scoreText = document.getElementById("scoreText");
    scoreText.innerHTML = makeTrailingZeros(Math.floor(time / 200));
    
}

function makeTrailingZeros(num) {
    if (num > 999) {
        return "" + num;
    }
    var s = "000" + num;
    return s.substr(s.length - 4);
}

function checkCollisionX() {
    let x;
    if (gamePlayMovedTimes < 10) {
        x = 1;
    } else if (gamePlayMovedTimes < 25) {
        x = 2;
    } else if (gamePlayMovedTimes < 50) {
        x = 3;
    } else {
        x = 5;
    }

    let yTopObstacle = heights[4] * 30;
    let player = document.getElementById("player");
    let yBottomPlayer = parseInt((player.style.bottom).replace("px", ""));
    

    let xLeftObstacle = 30 * 4 - gamePlayPossition;
    let xRightPlayer = 30 * 3 + x;

    if (xLeftObstacle > xRightPlayer && yBottomPlayer < yTopObstacle) {
        return true;
    } else {
        return false;
    }
}

function initKeyMap() {
    
    for (k of jumpKeys) {
        keys.set(k, false);
    }

    for (k of fallKeys) {
        keys.set(k, false);
    }

}

function addListeners() {

    document.addEventListener('keydown', function(event) {

        if (!keys.has(event.code)) {
            return;
        }
        
        if (jumpKeys.includes(event.code)) {
            jump = true;
            console.log(event.code + " pressed; jump = true")
        }

        if (fallKeys.includes(event.code)) {
            fall = true;
            console.log(event.code + " pressed; fall = true")
        }

        keys[event.code] = true;
    });

    document.addEventListener('keyup', function(event) {

        if (!keys.has(event.code)) {
            return;
        }

        if (jumpKeys.includes(event.code) && keys[event.code]) {
            let release = true;
            console.log('jump key check');
            for (k of jumpKeys) {
                console.log(k);
                if (k != event.code && keys[k]) {
                    console.log('key is pressed');
                    release = false;
                    break;
                }
            }
            jump = !release;
            console.log(event.code + " released; jump = " + jump)
        }

        if (fallKeys.includes(event.code) && keys[event.code]) {
            let release = true;

            for (k of fallKeys) {
                if (k != event.code && keys[k]) {
                    release = false;
                    break;
                }
            }

            fall = !release;
            console.log(event.code + " released; fall = " + fall)
        }

        keys[event.code] = false;
    })
}


initKeyMap();
addListeners();
let intervalId = setInterval(start, interval);
